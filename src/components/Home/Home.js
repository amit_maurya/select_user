import React, { Component } from 'react'
import './Home.css';
import Data from '../../assets/Data.json'

class Home extends Component {
    state = {
        open: false,
        selectedUser: null,
        id: null,
        activeUser: null,
    }

    componentDidMount() {
        console.log('data', Data)
    }

    popupModal = () => {
        this.setState({
            open: !this.state.open,
        });
    }

    selectHandler = (value) => {
        if (value !== 'Select Users') {
            this.setState({ id: value }, () => {
                let user = Data.members.filter((val) => val.id === this.state.id);
                this.setState({ activeUser: user[0].real_name })
                this.popupModal();
            });
        }
    }

    selectActivityHandler = (value) => {

        let user = Data.members.filter((val) => val.id === this.state.id);
        console.log('user', user)
        this.setState({ selectedUser: user });

    }

    selectDate = (date) => {
        console.log('selectDate', date.target.value)
        var dt = new Date('Mar 1 2020  11:11AM');
        console.log('dt', dt)
    }

    render() {
        const openClass = `modal ${this.state.open ? "show-modal" : ""}`;
        return (
            <div className="style-top">
                <h1>Users List</h1>
                {Data.members ? Data.members.map((val) => {
                    return (
                        <div key={val.id} className="user-List"
                            onClick={() => this.selectHandler(val.id)} >
                            <span >{val.real_name}
                            </span></div>
                    )
                }) : null}

                {/* HTML POPUP MODAL */}
                <div className={openClass}>
                    <div className="modal-content">
                        <span className="close-button" onClick={this.popupModal}>
                            X
                            </span>
                        <span style={{ textAlign: 'center' }}>
                            <h3> {'Users Activity: '}{this.state.activeUser && this.state.activeUser}</h3>
                        </span><br />

                        <select onChange={(event) => this.selectActivityHandler(event.target.value)}>
                            <option>Select Activity</option>
                            <option>Select All Activity</option>
                        </select><br />
                        {this.state.selectedUser ? this.state.selectedUser[0].activity_periods.map((val) => {
                            console.log('val', val.start_time)
                            return (
                                <div className="date-list">
                                    <div key={val.start_time}>{'Start Date: ' + val.start_time}</div>
                                    <div key={val.end_time}>{'End Date: ' + val.end_time}</div>
                                </div>
                            )
                        }) : null}

                        <div className='date-list'>
                            <span>Select Activity by Date:</span>&nbsp;&nbsp;
                        <input type="date" onChange={(event) => this.selectDate(event)}></input>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default Home;